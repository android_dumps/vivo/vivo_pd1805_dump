#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:67108864:a0585a8e8ea735f27632f26770f3dc848c188166; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:67108864:d3a592e7fcf9290933e1b636744ddff463605ac0 EMMC:/dev/block/bootdevice/by-name/recovery a0585a8e8ea735f27632f26770f3dc848c188166 67108864 d3a592e7fcf9290933e1b636744ddff463605ac0:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
